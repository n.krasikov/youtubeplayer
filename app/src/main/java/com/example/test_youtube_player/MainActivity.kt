package com.example.test_youtube_player

import android.os.Bundle
import android.widget.Toast
import com.example.test_youtube_player.databinding.ActivityMainBinding
import com.google.android.youtube.player.*

class MainActivity : YouTubeBaseActivity() {

    private lateinit var binding: ActivityMainBinding

    private val API_KEY = "AIzaSyAcfyFDMT8ESYb_LvrprwwUgGnG76X73GA"

    private val VIDEO_URL = "36YnV9STBqc"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val listener = object: YouTubePlayer.OnInitializedListener {

            override fun onInitializationFailure(
                provider: YouTubePlayer.Provider?,
                result: YouTubeInitializationResult?
            ) {
                Toast.makeText(applicationContext, "Initialization Failure", Toast.LENGTH_LONG).show()
            }

            override fun onInitializationSuccess(
                provider: YouTubePlayer.Provider?,
                youTubePlayer: YouTubePlayer?,
                wasRestored: Boolean
            ) {
                youTubePlayer?.loadVideo(VIDEO_URL)
                youTubePlayer?.play()
            }
        }

        binding.player.initialize(API_KEY, listener)
    }


}